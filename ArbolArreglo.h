#include <iostream>
#include "Pila.h"
//#include "Colas.h"
#ifndef ARBOLARREGLO_H
#define ARBOLARREGLO_H

using namespace std;

struct nodo{
  int dato;
  int izq;
  int der;
};

class Arbol {
 	int tam;
  	Pila<int> rec;
  	nodo arreglo[];
	public:
  		//CONSTRUCTOR
  		Arbol() {
  			Arbol(20);
  		}
  		Arbol(int tamano);

  		//FUNCIONES PUBLICAS
  		bool insertar(int tamano);
  		bool insertar1(int tamano);
  		bool insertarRaiz(int tamano);
  		bool eliminar(int tamano);
  		void impArbolPreOrden();
  		void impArbolInOrden();
  		void impArbolPosOrden();

  	//FUNCIONES UTILIZADAS DENTRO DE LA CLASE QUE NO DEBEN DE SER CONOCIDAS POR QUIEN USE LA LIBRERIA
	private:
  		int getPadrePosicion(int numero);
  		int derechoMasIzq(int posicion);


};

Arbol::Arbol(int tamano){
  tam = tamano;
  nodo arreglo[tamano];
  //Se inicializael arreglo para que se apunte al siguiente vacio en todas las posiciones
  for (int j = 0; j < tamano; j++) {
    //MIENTRAS QUE NO SEA LA ULTIMA POSICION
    if(j+1 != tamano){
      arreglo[j].der = j+1;
      arreglo[j].izq = 0;
      
    //SI ES LA ULTIMA POSICION
    }else{
      arreglo[j].der = 0;
      arreglo[j].izq = 0;
    }
  }
}


bool Arbol::insertar(int numero){

  	//POSICION QUE CAMBIARA EN BUSCA UNA POSICION LIBRE :v
	int posicionActual = 0;
  	//Se busca la raiz
  	posicionActual = arreglo[0].izq;

  	for (int i = 0;;) {
	    //El arreglo esta lleno
	    //if(arreglo[posicionActual].der == 0){
	    //	return 0;
	    //}
	    //EN CASO DE QUE EL NUMERO SEA MAYOR
	    if (numero > arreglo[posicionActual].dato){
	    	//SI EL NODO NO TIENE MAS HIJOS EN  LA DER
	    	if(arreglo[posicionActual].der == 0){	

		        //SE ENLAZA A EL PADRE CON SU HIJO
		        arreglo[posicionActual].der = arreglo[0].der;
		        //Se actualiza la cabecera
		        arreglo[0].der = arreglo[arreglo[posicionActual].der].der;
		        //SE LLENA AL HIJO CON SUS RESPECTIVOS VALORES
		        arreglo[arreglo[posicionActual].der].dato = numero;
		        
		        arreglo[arreglo[posicionActual].der].izq = 0;
		        arreglo[arreglo[posicionActual].der].der = 0;
		        return 1;
        		// SI EL NODO TIENE MAS HIJOS SE SIGUE BAJANDO
      		}else{
        		posicionActual = arreglo[posicionActual].der;
      		}
      	//SI EL NUMERO ES MENOR QUE EL NODO
    	}else if (numero < arreglo[posicionActual].dato){
	      	//SI EL NODO NO TIENE MAS HIJOS EN LA IZQ
	      	if(arreglo[posicionActual].izq == 0){
		        //SE ENLAZA A EL PADRE CON SU HIJO
		        arreglo[posicionActual].izq = arreglo[0].der;
		        //Se actualiza la cabecera
		        arreglo[0].der = arreglo[arreglo[posicionActual].izq].der;
		        //SE LLENA AL HIJO CON SUS RESPECTIVOS VALORES
		        arreglo[arreglo[posicionActual].der].dato = numero;
		        arreglo[arreglo[posicionActual].der].izq = 0;
		        arreglo[arreglo[posicionActual].der].der = 0;
		        //SE SALE DEL Ciclos
		        return 1;
	      	}else{
	    		posicionActual = arreglo[posicionActual].izq;
     		}
    	}
    }

  	//Si de alguna manera llega a terminar ocurrio un error
  	return -1;
}
 //SE ELIMINA UN ELEMENTO DEL ARBOL
bool Arbol::eliminar(int numero){
  //Posicion del elemento a eliminar
  int posEliminar;
  //Posicion del padre del elemento a eliminar
  int posPadre = getPadrePosicion(numero);
  int aux;
  //Se busca el padre de el numero y dependiendo si el numero es el izquierdo o derecho pues lo asumira posEliminar
  //SE DEFINE LA POSICION A ELIMINAR
  if(arreglo[posPadre].izq == numero){
    posEliminar = arreglo[posPadre].izq;
  }else if(arreglo[posPadre].der == numero){
    posEliminar = arreglo[posPadre].der;
  }

  //SI NO TIENE HIJOS
  if (arreglo[posEliminar].der == 0 && arreglo[posEliminar].izq == 0 ){
    //SE ELIMINA EL NODO HACIENDO VISIBLE COMO UN VACIO EN CONTROL, LA INFO QUE QUEDA ES BASURA
    arreglo[posEliminar].der = arreglo[0].der;
    arreglo[0].der = posEliminar;
    //SI HAY UN HIJO A LA IZQ
  }else if(arreglo[posEliminar].der == 0 && arreglo[posEliminar].izq != 0){
    if (arreglo[posPadre].izq == posEliminar){
      arreglo[posPadre].izq =arreglo[posEliminar].izq;
    }else if (arreglo[posPadre].der == posEliminar){
      arreglo[posPadre].der =arreglo[posEliminar].izq;
    }
    // SI HAY UN HIJO A LA DERECHA
  }else if(arreglo[posEliminar].der != 0 && arreglo[posEliminar].izq == 0){
    if (arreglo[posPadre].izq == posEliminar){
      arreglo[posPadre].izq = arreglo[posEliminar].der;
    }else if (arreglo[posPadre].der == posEliminar){
      arreglo[posPadre].der = arreglo[posEliminar].der;
    }
  //SI SON DOS HIJOS
  }else if(arreglo[posEliminar].der != 0 && arreglo[posEliminar].izq != 0){
    //Se obtiene el elemento derecho mas a a la izquierda
    //OJO ES UN NUMERO NO POSICION
    int resDerIzq = derechoMasIzq(posEliminar);

    //Se elimina el numero
    eliminar(resDerIzq);
    //Se reemplaza en la posicion a eliminar
    arreglo[posEliminar].dato = resDerIzq;

  }
  return 0;
}

//Se obtiene el padre de un numero dado
int Arbol::getPadrePosicion(int numero){
  //Se va a la raiz
  int posActual=arreglo[0].der;
  int izq;
  int der;

  //UN CICLO INFINITO
  for (int i = 0;;) {
    izq = arreglo[posActual].izq;
    der = arreglo[posActual].der;
    if(arreglo[izq].dato == numero || arreglo[der].dato == numero){
      //SE RETORNA LA POSICION EN CASO DE SER ENCONTRADO
      return posActual;
    }
    if(posActual == 0){
      //EL NUMERO NO EXISTE
      return -1;
    }
    //SI NO SE ENCUENTRA Y NO A LLEGADO AL FINAL VA AL SIGUIENTE NODO
    posActual = arreglo[posActual].der;
  }
}

//Se busca el derecho mas izquierdo de una posicion dada
int Arbol::derechoMasIzq(int posicion){
  int pos = posicion;
  pos = arreglo[pos].der;
  for (int i = 0;;) {
    if(arreglo[pos].izq == 0){
      return pos;
    }else{
      pos = arreglo[pos].izq;
    }
  }
  return -1;
}

bool Arbol::insertar1(int numero){
	int posVac = arreglo[0].der; // Se obtiene la primera posicion vacia
	int posRaiz = arreglo[0].izq;
	//Si la sig posicion vacia es cero significa que el arbol esta lleno
	if(posVac!=0){
		arreglo[posVac].dato=numero;
		arreglo[0].der=arreglo[posVac].der;
		arreglo[posVac].der=0;
		for(int i=0;;){
			if(numero<arreglo[posRaiz].dato){
				if(arreglo[posRaiz].izq==0){
					arreglo[posRaiz].izq=posVac;
					return 1;
				}else{
					posRaiz = arreglo[posRaiz].izq;
				}	
			}else if(numero>arreglo[posRaiz].dato){
				if(arreglo[posRaiz].der==0){
					arreglo[posRaiz].der=posVac;
					return 1;
				}else{
					posRaiz = arreglo[posRaiz].der;
				}	
			}
			
		}
	}
	
}


bool Arbol::insertarRaiz(int numero){
	arreglo[0].izq=1; // La raiz va a estar en la pos 1
	arreglo[1].dato=numero;
	arreglo[1].der=0;
	arreglo[1].izq=0;
	arreglo[0].der=2; // Se actualiza la sig posicion vacia
}

void Arbol::impArbolInOrden(){

	for (int i = 0 ; i<10 ;i++){
		cout << arreglo[i].dato <<endl;
	
	}
    
}


        
void Arbol::impArbolPreOrden(){
	int raiz, final;
    raiz = arreglo[0].izq;
    final = arreglo[0].izq;
    while (arreglo[final].der != 0) {
        final = arreglo[final].der;
    }
    while (raiz != 0) {
        if (arreglo[raiz].izq != 0) {
            cout << arreglo[raiz].dato << " ";
            rec.push(raiz);
            raiz = arreglo[raiz].izq;
        } else {
            cout << arreglo[raiz].dato << " ";
            if (arreglo[raiz].der != 0) {
                cout << arreglo[arreglo[raiz].der].dato << " ";
            }
            raiz = rec.pop();
            while (arreglo[raiz].der == 0) {
                raiz = rec.pop();
            }
            raiz = arreglo[raiz].der;
        }
        if (raiz == final) {
            cout << arreglo[raiz].dato << endl;
            raiz = 0;
        }
    }
}
#endif